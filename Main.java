import java.util.Scanner;

public class Main{
    public static void main(String[] args) {
        Converter converter = new Converter();
        String[] math_operation_array = {"+", "-", "/", "*"};
        String[] regex_array = {"\\+", "-", "/", "\\*"};
        Scanner scn = new Scanner(System.in);
        System.out.print("Введите выражение: ");
        String exp = scn.nextLine();
        //Определяем арифметическое действие:
        int arrIndex=0;
        for(int i = 0; i < math_operation_array.length; i++) {
            if(exp.contains(math_operation_array[i])){
                arrIndex = i;
                break;
            }
            //Если не нашли арифметического действия, завершаем программу

            else{
                System.out.println("Некорректное выражение");
                return;
            }
        }

        //Делим строчку по найденному арифметическому знаку


        String[] data = exp.split(regex_array[arrIndex]);
        //Определяем, находятся ли числа в одном формате (оба римские или оба арабские)
        if(converter.isRoman(data[0]) == converter.isRoman(data[1])){
            int num_1,num_2;
            //Определяем, римские ли это числа
            boolean isRoman = converter.isRoman(data[0]);
            if(isRoman){
                //если римские, то конвертируем их в арабские

                num_1 = converter.romanToInt(data[0]);
                num_2 = converter.romanToInt(data[1]);

            }
            else{
                //если арабские, конвертируем их из строки в число
                num_1 = Integer.parseInt(data[0]);
                num_2 = Integer.parseInt(data[1]);
            }
            //выполняем с числами арифметическое действие
            int result;
            switch (math_operation_array[arrIndex]){
                case "+":
                    result = num_1+num_2;
                    break;
                case "-":
                    result = num_1-num_2;
                    break;
                case "*":
                    result = num_1*num_2;
                    break;
                default:
                    result = num_1/num_2;
                    break;
            }
            //15->XV
            if(isRoman){
                //если числа были римские, возвращаем результат в римском числе
                System.out.println("Ответ: " + converter.intToRoman(result));
            }
            else{
                //если числа были арабские, возвращаем результат в арабском числе
                System.out.print("Ответ: " + result);
            }
        }else{
            System.out.println("Числа должны быть в одном формате");
        }


    }
}